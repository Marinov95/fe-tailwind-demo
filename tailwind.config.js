/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: ["class"],
  content: [
    './pages/**/*.{ts,tsx}',
    './components/**/*.{ts,tsx}',
    './app/**/*.{ts,tsx}',
    './src/**/*.{ts,tsx}',
  ],
  prefix: "",
  theme: {
    container: {
      center: true,
      padding: {
        DEFAULT: "1rem",
        sm: "1.5rem",
        lg: "2rem",
        xl: "2.5rem",
        "2xl": "3rem",
      },
    },
    extend: {
      colors: {
        primary: {
          DEFAULT: "#26A1A9",
          50: "#BFEEF1",
          100: "#AEE9ED",
          200: "#8DE0E5",
          300: "#6CD7DE",
          400: "#4ACED6",
          500: "#2DC1CA",
          600: "#26A1A9",
          700: "#1C757B",
          800: "#114A4D",
          900: "#071E20",
          950: "#020809",
        },
        secondary: "#EAEAEA",
        alternative: {
          DEFAULT: "#53A856",
          50: "#E8F4E8",
          100: "#DAEDDB",
          200: "#BFDFC0",
          300: "#A4D2A5",
          400: "#88C48A",
          500: "#6DB770",
          600: "#53A856",
          700: "#408243",
          800: "#2E5D30",
          900: "#1B371C",
          950: "#122513",
        },
      },
      keyframes: {
        "accordion-down": {
          from: { height: "0" },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: "0" },
        },
      },
      animation: {
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
      },
    },
  },
  mode: 'jit',
  purge: [
    './public/**/*.html',
    './src/**/*.{js,jsx,ts,tsx,vue}',
  ],
  plugins: [require("tailwindcss-animate")],
}