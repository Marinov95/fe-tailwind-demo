import { useContext } from "react";
import Card from "./components/Card";
import CustomCheckbox from "./components/CustomCheckbox";
import useCheckbox from "./hooks/useCheckbox";
import PreferenceContext from "./store/PreferenceContext";
import DarkModeToggle from "./components/DarkModeToggle";
import clsx from "clsx";
import Button from "./components/Button";
import Pitfall from "./components/Pitfall";
import Alert from "./components/Alert";
import { FaInfo } from "react-icons/fa";
import NotificationMessage from "./components/NotificationMessage";

const COLOR_MAP = {
	red: "bg-red-200",
	blue: "bg-blue-200",
};

function App() {
	const { prefersDarkMode } = useContext(PreferenceContext);
	const checkboxState = useCheckbox({ initialChecked: false });

	const cardContent = clsx("Hello", "World");
	return (
		<div className={clsx(prefersDarkMode && "dark")}>
			<div className="min-h-[100dvh] bg-gray-200 p-5 dark:bg-black/80">
				<div className="container space-y-10">
					<div className="grid grid-cols-1 gap-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
						<Card>{cardContent}</Card>
						<Card>{cardContent}</Card>
						<Card>{cardContent}</Card>
						<Card className="bg-secondary text-alternative-600 dark:text-black">
							{cardContent}
						</Card>
					</div>

					<Alert theme="information" text="Hover me" icon={<FaInfo />} />

					<CustomCheckbox state={checkboxState} label="Do you like tailwind?" />

					<Button>Verbosity</Button>
					<Pitfall />
					<div>
						<DarkModeToggle />
						<NotificationMessage
							message="You are in dark mode"
							isShowing={prefersDarkMode}
						/>
					</div>

					<div className="bg-gradient-to-r from-red-500 to-blue-500 p-4"></div>
				</div>
			</div>
		</div>
	);
}

export default App;

