import { FiCheck } from "react-icons/fi";
import clsx from "clsx";
import { Field, Label, Switch } from "@headlessui/react";
import { CheckboxInputReturnType } from "../hooks/useCheckbox";

type Props = {
  state: CheckboxInputReturnType;
  label?: string;
};

const CustomCheckbox = ({ state, label = "" }: Props) => {
  return (
    <Field>
      <div
        className={clsx(
          "flex w-fit items-center gap-2 rounded-[5px] bg-transparent p-1 shadow-sm transition-all duration-200",
        )}
      >
        <Switch
          checked={state.checked}
          onChange={state.onChange}
          className="peer"
        >
          <FiCheck
            className={clsx(
              "border border-primary text-xl transition-all duration-300 ease-out",
              state.checked
                ? "border-opacity-100 text-primary"
                : "text-transparent",
            )}
          />
        </Switch>
        {label && (
          <Label className="cursor-pointer transition-colors duration-200 peer-aria-checked:text-primary dark:text-white">
            {label}
          </Label>
        )}
      </div>
    </Field>
  );
};

export default CustomCheckbox;
