import { useContext } from "react";
import { Switch } from "@headlessui/react";
import PreferenceContext from "../store/PreferenceContext";
import clsx from "clsx";

const DarkModeToggle = () => {
  const { prefersDarkMode, handleDarkModeChange } =
    useContext(PreferenceContext);

  return (
    <Switch
      checked={prefersDarkMode}
      onChange={handleDarkModeChange}
      className={clsx(
        prefersDarkMode ? "bg-primary-950" : "bg-slate-300",
        "relative inline-flex h-[30px] w-[58px] shrink-0 cursor-pointer rounded-full border-2 border-transparent transition-colors duration-200 ease-in-out focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75",
      )}
    >
      <span className="sr-only">Use setting</span>
      <span
        aria-hidden="true"
        className={clsx(
          "pointer-events-none inline-block h-[26px] w-[26px] transform rounded-full shadow-lg ring-0 transition duration-200 ease-in-out",
          prefersDarkMode
            ? "translate-x-9 bg-white"
            : "translate-x-0 bg-primary-950",
        )}
      />
    </Switch>
  );
};
export default DarkModeToggle;
