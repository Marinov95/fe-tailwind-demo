import { twMerge } from "tailwind-merge";

type Props = {
	children: React.ReactNode;
	className?: string;
};

const Card = ({ children, className }: Props) => {
	return (
		<div
			className={twMerge(
				"rounded-lg bg-primary-200 p-2 shadow-md md:p-3 lg:p-4 xl:p-5 dark:bg-primary dark:text-white",
				className && className,
			)}
		>
			{children}
		</div>
	);
};

export default Card;
