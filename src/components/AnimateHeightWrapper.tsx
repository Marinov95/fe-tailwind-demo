import { Transition } from "@headlessui/react";
import { twMerge } from "tailwind-merge";

type Props = {
	className?: string;
	children: React.ReactNode;
	isShowing: boolean;
};

const AnimateHeightWrapper = ({ className, children, isShowing }: Props) => {
	return (
		<Transition
			as="div"
			show={isShowing}
			//   appear={true}
			enter="transform transition-all duration-150 origin-top"
			enterFrom="opacity-0 grid-rows-[0fr]"
			enterTo="opacity-100 grid-rows-[1fr]"
			leave="transform duration-150 transition-all origin-bottom"
			leaveFrom="opacity-100 grid-rows-[1fr]"
			leaveTo="opacity-0 grid-rows-[0fr]"
			className={twMerge("relative grid", className && className)}
		>
			<div className="overflow-hidden"> {children}</div>
		</Transition>
	);
};

export default AnimateHeightWrapper;
