import clsx from "clsx";
import AnimateHeightWrapper from "./AnimateHeightWrapper";

type Props = {
	type?: "notification" | "success" | "error";
	isShowing: boolean;
	message?: string;
};

const NotificationMessage = ({
	type = "notification",
	isShowing,
	message,
}: Props) => {
	return (
		<>
			<AnimateHeightWrapper
				isShowing={isShowing}
				className={clsx(
					type === "notification" && "text-base text-black dark:text-white",
					type === "error" &&
						"text-[10px] leading-[10px] text-red-500 after:content-[''] sm:text-[13px] sm:leading-[13px]",
					type === "success" && "text-alternative-500",
				)}
			>
				{message === undefined || message === "" ? " " : message}
			</AnimateHeightWrapper>
		</>
	);
};

export default NotificationMessage;
