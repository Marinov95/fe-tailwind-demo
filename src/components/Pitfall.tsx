import { useState } from "react";

function Pitfall() {
	const [color, setColor] = useState("red");

	const onClick = () => setColor(color === "blue" ? "red" : "blue");

	return (
		<p className={`bg-${color}-200`} onClick={onClick}>
			Click here to toggle background color.
		</p>
	);
}

export default Pitfall;
