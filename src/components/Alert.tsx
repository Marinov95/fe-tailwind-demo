import clsx from "clsx";

type Props = {
	theme: "primary" | "information" | "danger";
	icon?: JSX.Element;
	text?: string;
} & JSX.IntrinsicElements["div"];

const Alert = ({ theme = "primary", icon, text, ...alertProps }: Props) => {
	return (
		<div
			className={clsx(
				"group relative flex flex-row items-center justify-between rounded p-2",
				theme === "primary" && "bg-primary text-white",
				theme === "information" && "bg-primary text-white",
				theme === "danger" && "bg-red-500 text-white",
			)}
			{...alertProps}
		>
			<p>{text}</p>
			<span className="me-2 group-hover:animate-spin">{icon}</span>
		</div>
	);
};

export default Alert;
