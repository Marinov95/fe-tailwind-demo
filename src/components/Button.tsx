import clsx from "clsx";
import { Link, LinkProps } from "react-router-dom";

type BaseProps = {
  theme?: "primary" | "secondary" | "delete" | "transparent" | "reset";
  icon?: JSX.Element;
  children?: React.ReactNode;
  as?: "a" | "Link" | "button";
};

type ButtonProps = BaseProps & JSX.IntrinsicElements["button"];

type ButtonLinkProps = BaseProps & JSX.IntrinsicElements["a"];

type ButtonRouterLinkProps = BaseProps & LinkProps;

type Props = ButtonLinkProps | ButtonRouterLinkProps | ButtonProps;

const BUTTON_CLASSES =
  "relative z-10 flex w-fit rounded-md enabled:before:rounded-md items-center justify-center gap-2 px-3 py-2 shadow-sm transition-all duration-300 enabled:before:absolute enabled:before:inset-0 enabled:before:-z-10 enabled:before:origin-right enabled:before:scale-x-0 enabled:before:bg-black/20 enabled:before:transition-transform enabled:before:duration-300 enabled:before:content-[''] enabled:before:hover:origin-left enabled:before:hover:scale-x-100 disabled:bg-gray-400 disabled:text-black/50";

const LINK_CLASSES =
  "relative z-10 flex w-fit rounded-md before:rounded-md items-center justify-center gap-2 px-3 py-2 shadow-sm transition-all duration-300 before:absolute before:inset-0 before:-z-10 before:origin-right before:scale-x-0 before:bg-black/20 before:transition-transform before:duration-300 before:content-[''] before:hover:origin-left before:hover:scale-x-100";

const Button = ({
  theme = "primary",
  icon,
  children,
  as = "button",
  ...props
}: Props) => {
  const baseClasses = clsx(
    theme === "primary" && "bg-primary text-white",
    theme === "secondary" && "bg-gray-300 text-black",
    theme === "delete" && "bg-red-500 text-white",
    theme === "transparent" && "bg-transparent text-black",
    theme === "reset" && "bg-alternative text-white",
  );

  if (as === "a") {
    const { ...anchorProps } = props as ButtonLinkProps;
    return (
      <a className={clsx(LINK_CLASSES, baseClasses)} {...anchorProps}>
        {icon && <span className="text-lg">{icon}</span>}
        <span>{children}</span>
      </a>
    );
  } else if (as === "Link") {
    const { ...linkProps } = props as ButtonRouterLinkProps;
    return (
      <Link className={clsx(LINK_CLASSES, baseClasses)} {...linkProps}>
        {icon && <span className="text-lg">{icon}</span>}
        <span>{children}</span>
      </Link>
    );
  } else {
    const { ...buttonProps } = props as ButtonProps;
    return (
      <button className={clsx(BUTTON_CLASSES, baseClasses)} {...buttonProps}>
        {icon && <span className="text-lg">{icon}</span>}
        <span>{children}</span>
      </button>
    );
  }
};

export default Button;
