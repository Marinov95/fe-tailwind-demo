import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import { PreferenceContextProvider } from "./store/PreferenceContext.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
	<React.StrictMode>
		<PreferenceContextProvider>
			<App />
		</PreferenceContextProvider>
	</React.StrictMode>
);
