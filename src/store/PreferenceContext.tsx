import React, { createContext, useState, useEffect } from "react";

const PreferenceContext = createContext({
	prefersDarkMode: false,
	handleDarkModeChange: () => {},
});

const LOCAL_STORAGE_KEYS = {
	prefersDarkMode: "prefersDarkMode",
};

export const PreferenceContextProvider = ({
	children,
}: {
	children: React.ReactNode;
}) => {
	const [prefersDarkMode, setPrefersDarkmode] = useState<boolean>(
		localStorage.getItem(LOCAL_STORAGE_KEYS.prefersDarkMode) !== null
			? JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEYS.prefersDarkMode)!)
			: false
	);

	const handleDarkModeChange = () => {
		setPrefersDarkmode((prev) => !prev);
		localStorage.setItem(
			LOCAL_STORAGE_KEYS.prefersDarkMode,
			JSON.stringify(!prefersDarkMode)
		);
	};

	useEffect(() => {
		// Add an event listener to handle changes in local storage values from other tabs/windows
		const handleStorageChange = (e: StorageEvent) => {
			if (e.key === LOCAL_STORAGE_KEYS.prefersDarkMode && e.newValue !== null) {
				setPrefersDarkmode(JSON.parse(e.newValue));
			}
		};

		window.addEventListener("storage", handleStorageChange);

		return () => {
			window.removeEventListener("storage", handleStorageChange);
		};
	}, []);

	return (
		<PreferenceContext.Provider
			value={{
				prefersDarkMode,
				handleDarkModeChange,
			}}
		>
			{children}
		</PreferenceContext.Provider>
	);
};

export default PreferenceContext;
