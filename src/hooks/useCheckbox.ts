import { useState } from "react";

export type CheckboxInputReturnType = {
  checked: boolean;
  hasChanged: boolean;
  reset: () => void;
  isValid: boolean;
  onChange: () => void;
};

const isValid = true;
type Props = {
  initialChecked: boolean;
  onChange?: () => void;
};
const useCheckbox = ({
  initialChecked = true,
  onChange,
}: Props): CheckboxInputReturnType => {
  const [checked, setChecked] = useState(initialChecked);

  const hasChanged = checked !== initialChecked;

  const reset = () => {
    setChecked(initialChecked);
  };
  const handleChange = () => {
    onChange !== undefined && onChange();
    setChecked(prevChecked => !prevChecked);
  };

  return {
    checked,
    hasChanged,
    reset,
    isValid,
    onChange: handleChange,
  };
};

export default useCheckbox;
